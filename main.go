package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/requirements", requirementsHandler)

	fs := http.FileServer(http.Dir("../base"))
	http.Handle("/", fs)

	fmt.Println("Serving...")
	log.Fatal(http.ListenAndServe(":714", nil))
}

func requirementsHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Request to requirementsHandler.")
	fmt.Fprint(w, readRequirementsFile())
}

func readRequirementsFile() string {
	dat, err := ioutil.ReadFile("requirements.json")
	if err != nil {
		panic(err)
	}

	return string(dat)
}
